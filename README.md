# Read Me Document

## Prerequisite
* Install NodeJS from https://nodejs.org
* Install Nativefier from https://github.com/jiahaog/nativefier

## Installing This App
You can just clone this repo and or make and run the 'create_app.sh' script. After satisfying the prerequisites.


### Resources Used
* [NodeJS](https://nodejs.orgl)
* [Nativefier](https://github.com/jiahaog/nativefier)
* [JavaScript for dark mode](https://userstyles.org/styles/170705/discord-dark-theme-for-glip)

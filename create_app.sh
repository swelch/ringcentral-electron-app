#!/bin/bash

nativefier --name 'RingCentral' --app-version '1.5' --icon RingCentral-icon.png --counter --internal-urls "(.*ringcentral.*)"  --single-instance --tray https://app.ringcentral.com
